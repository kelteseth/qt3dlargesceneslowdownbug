#include "RootEntity.h"
#include "GroundEntity.h"
#include "CylinderEntity.h"
#include <Qt3DCore/QNode>
#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DInput/QInputSettings>
#include <Qt3DLogic/QFrameAction>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QCameraLens>
#include <Qt3DRender/QPickingSettings>
#include <Qt3DRender/QRenderSettings>
#include <math.h>

namespace ibh {
RootEntity::RootEntity(Qt3DCore::QNode* parent)
    : Qt3DCore::QEntity(parent)
{
    // Notify when all properties are set in qml
    if (!connect(this, &RootEntity::completed, this, &RootEntity::onCompleted))
        assert(0);
}

Qt3DRender::QCamera* RootEntity::camera() const
{
    return m_camera;
}

Layers* RootEntity::layers() const
{
    return m_layers;
}

void RootEntity::onCompleted()
{
    init();
}

void RootEntity::setCamera(Qt3DRender::QCamera* camera)
{
    if (m_camera == camera)
        return;

    m_camera = camera;
    emit cameraChanged(m_camera);
}

void RootEntity::setLayers(Layers* layers)
{
    if (m_layers == layers)
        return;

    m_layers = layers;
    emit layersChanged(m_layers);
}

void RootEntity::init()
{
    auto* ground = new GroundEntity(this);
    ground->setCamera(m_camera);
    ground->setLayers(m_layers);

    const int count = 10000;
    const int f = static_cast<int>(std::sqrt(count));
    const int d = 5;
    const float height = 2.0f;
    const float diameter = 1.0f;

    for (int idx = 0; idx < count; ++idx) {

        QVector3D position((idx * d) % (f * d), idx / f * d, 1.0f);

        auto* cylinder = new CylinderEntity(this);
        cylinder->addLayer(m_layers->objectLayer());
        cylinder->setMeshes(m_meshes.cylinderMeshes());
        cylinder->setMaterials(m_materials.materials());
        cylinder->setTranslation(position);
        cylinder->setDiameter(diameter);
        cylinder->setHeight(height);
        //cylinder->setLevelOfDetailEnabled(m_camera);
    }
}
}
