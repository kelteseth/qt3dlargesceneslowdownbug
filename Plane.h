#pragma once
#include "Intersectable.h"
#include <QVector3D>

namespace ibh {
class Ray;
class Plane : public Intersectable {
private:
    QVector3D _point;
    QVector3D _normal;

public:
    Plane() = default;
    Plane(const QVector3D& point, const QVector3D& normal);
    Plane(float a, float b, float c, float d);
    Plane(const QVector3D& p, const QVector3D& q, const QVector3D& r);
    QVector3D& getNormal() { return _normal; }
    const QVector3D& getNormal() const { return _normal; }
    QVector3D& getPoint() { return _point; }
    const QVector3D& getPoint() const { return _point; }
    float getDistance() const { return -QVector3D::dotProduct(_normal, _point); }
    void normalize();
    QVector3D intersect(const Ray& ray) const override;
};
}
