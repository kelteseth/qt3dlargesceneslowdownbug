#pragma once
#include <QVector3D>

namespace ibh {
class Ray {
private:
    QVector3D _origin;
    QVector3D _destination;
    QVector3D _direction;

public:
    Ray() = default;
    Ray(const QVector3D& origin, const QVector3D& destination)
        : _origin(origin)
        , _destination(destination)
        , _direction(_destination - _origin)
    {
    }
    QVector3D& getOrigin() { return _origin; }
    const QVector3D& getOrigin() const { return _origin; }
    QVector3D& getDestination() { return _destination; }
    const QVector3D& getDestination() const { return _destination; }
    QVector3D& getDirection() { return _direction; }
    const QVector3D& getDirection() const { return _direction; }
    QVector3D getPointOnRay(float alpha) const { return _origin + _direction * alpha; }
};
}
