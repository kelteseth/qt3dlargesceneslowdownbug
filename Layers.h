#pragma once
#include <Qt3DCore/QNode>
#include <Qt3DRender/QLayer>

namespace ibh {

class Layers : public Qt3DCore::QNode {
    Q_OBJECT
    Q_PROPERTY(Qt3DRender::QLayer* debugLayer READ debugLayer WRITE setDebugLayer NOTIFY debugLayerChanged)
    Q_PROPERTY(Qt3DRender::QLayer* sceneLayer READ sceneLayer WRITE setSceneLayer NOTIFY sceneLayerChanged)
    Q_PROPERTY(Qt3DRender::QLayer* objectLayer READ objectLayer WRITE setObjectLayer NOTIFY objectLayerChanged)
    Q_PROPERTY(Qt3DRender::QLayer* osmLayer READ osmLayer WRITE setOsmLayer NOTIFY osmLayerChanged)
    Q_PROPERTY(Qt3DRender::QLayer* dxfLayer READ dxfLayer WRITE setDxfLayer NOTIFY dxfLayerChanged)
    Q_PROPERTY(Qt3DRender::QLayer* flagLayer READ flagLayer WRITE setFlagLayer NOTIFY flagLayerChanged)

private:
    Qt3DRender::QLayer* m_debugLayer = nullptr;
    Qt3DRender::QLayer* m_sceneLayer = nullptr;
    Qt3DRender::QLayer* m_objectLayer = nullptr;
    Qt3DRender::QLayer* m_osmLayer = nullptr;
    Qt3DRender::QLayer* m_dxfLayer = nullptr;
    Qt3DRender::QLayer* m_flagLayer = nullptr;

public:
    explicit Layers(Qt3DCore::QNode* parent = nullptr);
    Qt3DRender::QLayer* debugLayer();
    Qt3DRender::QLayer* sceneLayer();
    Qt3DRender::QLayer* objectLayer();
    Qt3DRender::QLayer* osmLayer();
    Qt3DRender::QLayer* dxfLayer();
    Qt3DRender::QLayer* flagLayer();

public slots:
    void setDebugLayer(Qt3DRender::QLayer* debugLayer);
    void setSceneLayer(Qt3DRender::QLayer* sceneLayer);
    void setObjectLayer(Qt3DRender::QLayer* objectLayer);
    void setOsmLayer(Qt3DRender::QLayer* osmLayer);
    void setDxfLayer(Qt3DRender::QLayer* dxfLayer);
    void setFlagLayer(Qt3DRender::QLayer* flagLayer);

signals:
    void debugLayerChanged(Qt3DRender::QLayer* debugLayer);
    void sceneLayerChanged(Qt3DRender::QLayer* sceneLayer);
    void objectLayerChanged(Qt3DRender::QLayer* objectLayer);
    void osmLayerChanged(Qt3DRender::QLayer* osmLayer);
    void dxfLayerChanged(Qt3DRender::QLayer* dxfLayer);
    void flagLayerChanged(Qt3DRender::QLayer* flagLayer);
};

}
