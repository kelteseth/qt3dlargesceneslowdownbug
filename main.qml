import QtQuick 2.0
import QtQuick.Scene3D 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import Qt3D.Input 2.0

Item {
    id: root
    Scene3D {
        id: scene3d
        anchors.fill: parent
        anchors.margins: 10
        focus: true
        multisample: false
        aspects: ["input", "logic"]
        cameraAspectRatioMode: Scene3D.AutomaticAspectRatio
        hoverEnabled: false
        clip: true
        onWidthChanged: sceneEntity.viewport.width = width
        onHeightChanged: sceneEntity.viewport.height = height
        SceneEntity {
            id: sceneEntity
        }
    }

    FpsDisplay {
        id: fpsDisplay
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        width: 32
        height: 64
        hidden: false
    }

    Timer {
        interval: 1000
        repeat: true
        running: !fpsDisplay.hidden
        onTriggered: {
            fpsDisplay.fps = sceneEntity.frameCount
            sceneEntity.frameCount = 0
        }
        onRunningChanged: sceneEntity.frameCount = 0
    }

    Component.onCompleted: console.log("Scene3D loaded")
    Component.onDestruction: console.log("Scene3D destroyed")
}
