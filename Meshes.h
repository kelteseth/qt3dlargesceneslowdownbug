#pragma once
#include <QVector>

namespace Qt3DExtras {
class QCylinderMesh;
}
namespace ibh {
class LineMesh;

class Meshes {
private:
    Qt3DExtras::QCylinderMesh* m_cylinderHD = nullptr;
    Qt3DExtras::QCylinderMesh* m_cylinderMD = nullptr;
    Qt3DExtras::QCylinderMesh* m_cylinderLD = nullptr;
    LineMesh* m_line = nullptr;

public:
    Qt3DExtras::QCylinderMesh* cylinderHD();
    Qt3DExtras::QCylinderMesh* cylinderMD();
    Qt3DExtras::QCylinderMesh* cylinderLD();
    QVector<Qt3DExtras::QCylinderMesh*> cylinderMeshes();
    LineMesh* line();
};

}
