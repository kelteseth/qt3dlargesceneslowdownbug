#pragma once
#include <Qt3DRender/QGeometryRenderer>

class QNode;
namespace ibh {

// A line of length 1 along the X axis.
class LineMesh : public Qt3DRender::QGeometryRenderer {
public:
    LineMesh(Qt3DCore::QNode* parent = nullptr);
};

}
