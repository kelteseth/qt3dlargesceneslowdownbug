#include "Plane.h"
#include "Ray.h"

namespace ibh {
Plane::Plane(const QVector3D& point, const QVector3D& normal)
    : _point(point)
    , _normal(normal)
{
}

Plane::Plane(float a, float b, float c, float d)
    : _point({ 0, 0, -d / c })
    , _normal({ a, b, c })
{
}

Plane::Plane(const QVector3D& p, const QVector3D& q, const QVector3D& r)
    : _point(q)
    , _normal(QVector3D::crossProduct(r - q, p - q))
{
}

void Plane::normalize()
{
    const auto norm = _normal.length();
    if (norm == 0.0f)
        return;

    _normal /= norm;
    _point /= norm;
}

QVector3D Plane::intersect(const Ray& ray) const
{
    const auto& o = ray.getOrigin();
    const auto& d = ray.getDirection();

    const auto a = getDistance() - (_normal.x() * o.x()) - (_normal.y() * o.y()) - (_normal.z() * o.z());
    const auto b = (_normal.x() * d.x()) + (_normal.y() * d.y()) + (_normal.z() * d.z());

    if (qFuzzyCompare(b, 0.0f))
        return {}; // Keine Schnittpunkt. Ebene und Gerade sind parallel.

    const auto alpha = a / b;
    return ray.getPointOnRay(alpha);
}
}
