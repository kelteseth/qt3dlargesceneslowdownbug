QT += 3dcore 3drender 3dinput 3dlogic qml quick 3dquick 3dquickextras

CONFIG += c++17 console

SOURCES += \
    main.cpp \
    CameraRayCaster.cpp \
    Plane.cpp \
    Disc.cpp \    
    Materials.cpp \
    Meshes.cpp \
    LineMesh.cpp \
    Layers.cpp \
    RootEntity.cpp \
    MyCameraController.cpp \
    GroundEntity.cpp \
    CylinderEntity.cpp

RESOURCES += \
    LevelOfDetail.qrc

HEADERS += \
    CameraRayCaster.h \
    Ray.h \
    Plane.h \
    Disc.h \
    Intersectable.h \
    Materials.h \
    Meshes.h \
    LineMesh.h \
    Layers.h \    
    RootEntity.h \
    MyCameraController.h \
    GroundEntity.h \
    CylinderEntity.h

