#pragma once
#include <QQuaternion>
#include <QVector3D>
#include <Qt3DCore/QEntity>

namespace Qt3DCore {
class QNode;
class QEntity;
class QTransform;
}
namespace Qt3DRender {
class QLayer;
class QLevelOfDetail;
class QCamera;
}
namespace Qt3DExtras {
class QCylinderMesh;
class QPhongMaterial;
}
namespace ibh {
class Meshes;
class Materials;

class CylinderEntity : public Qt3DCore::QEntity {
    Q_OBJECT

private:
    QVector<Qt3DExtras::QCylinderMesh*> m_meshes;
    Qt3DExtras::QCylinderMesh* m_mesh = nullptr;
    Qt3DCore::QTransform* m_transform = nullptr;
    QVector<Qt3DExtras::QPhongMaterial*> m_materials;
    Qt3DExtras::QPhongMaterial* m_material = nullptr;
    Qt3DRender::QLevelOfDetail* m_levelOfDetail = nullptr;

public:
    CylinderEntity(Qt3DCore::QNode* parent = nullptr);
    void setMesh(Qt3DExtras::QCylinderMesh* mesh);
    void setMeshes(const QVector<Qt3DExtras::QCylinderMesh*>& meshes);
    void setMaterial(Qt3DExtras::QPhongMaterial* material);
    void setMaterials(const QVector<Qt3DExtras::QPhongMaterial*>& materials);
    void addLayer(Qt3DRender::QLayer* layer);
    void setTranslation(const QVector3D& translation);
    void move(const QVector3D& translation);
    void setRotation(const QQuaternion& rotation);
    void setDiameter(float diameter);
    void setHeight(float width);
    void setLevelOfDetailEnabled(Qt3DRender::QCamera* camera);
    Qt3DExtras::QCylinderMesh* getMesh() const;
    QVector3D getTranslation() const;
    QQuaternion getRotation() const;
    float getDiameter() const;
    float getHeight() const;

private:
    void init();
};
}
