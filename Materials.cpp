#include "Materials.h"
#include <Qt3DExtras/QPhongMaterial>

namespace ibh {
Qt3DExtras::QPhongMaterial* Materials::green()
{
    if (!_green) {
        _green = new Qt3DExtras::QPhongMaterial();
        _green->setDiffuse("green");
        //_green->setAmbient("green");
    }

    return _green;
}

Qt3DExtras::QPhongMaterial* Materials::red()
{
    if (!_red) {
        _red = new Qt3DExtras::QPhongMaterial();
        _red->setDiffuse("red");
        //_red->setAmbient("red");
    }

    return _red;
}

Qt3DExtras::QPhongMaterial* Materials::yellow()
{
    if (!_yellow) {
        _yellow = new Qt3DExtras::QPhongMaterial();
        _yellow->setDiffuse("yellow");
        //_yellow->setAmbient("yellow");
    }

    return _yellow;
}

Qt3DExtras::QPhongMaterial* Materials::blue()
{
    if (!_blue) {
        _blue = new Qt3DExtras::QPhongMaterial();
        _blue->setDiffuse("blue");
        //_blue->setAmbient("blue");
    }

    return _blue;
}

QVector<Qt3DExtras::QPhongMaterial*> Materials::materials()
{
    return { blue(), red(), green(), yellow() };
}
}
