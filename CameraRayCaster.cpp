#include "CameraRayCaster.h"
#include "Ray.h"
#include <Qt3DRender/QRayCasterHit>

namespace ibh {
Qt3DRender::QCamera* CameraRayCaster::camera() const
{
    return _camera;
}

void CameraRayCaster::setCamera(Qt3DRender::QCamera* camera)
{
    if (_camera == camera)
        return;

    if (camera && !camera->parent())
        camera->setParent(this);

    _camera = camera;

    emit cameraChanged();
}

QRect CameraRayCaster::viewport() const
{
    return _viewport;
}

void CameraRayCaster::setViewport(const QRect& viewport)
{
    if (_viewport == viewport)
        return;

    _viewport = viewport;
    emit viewportChanged();
}

Intersectable* CameraRayCaster::entity() const
{
    return _entity;
}

void CameraRayCaster::setEntity(Intersectable* entity)
{
    if (_entity == entity)
        return;

    _entity = entity;
    emit entityChanged();
}

void CameraRayCaster::trigger(const QPoint& pixel)
{
    if (!entity())
        return;

    Ray ray = createRay(pixel);
    auto intersect = _entity->intersect(ray);
    if (intersect.isNull()) {
        emit hitsChanged({});
        return;
    }

    const auto distance = (intersect - _camera->position()).length();
    Qt3DRender::QRayCasterHit hit(Qt3DRender::QRayCasterHit::HitType::PointHit,
        {}, distance, {}, intersect, 0, 0, 0, 0);

    emit hitsChanged({ hit });
}

Ray CameraRayCaster::createRay(const QPoint& pixel) const
{
    // Ursprung ist im Qt links oben, im OpenGL links unten
    // Das Zentrum der Kamra ist in der Mitte des Viewports
    const auto origin = QVector3D(_viewport.center());
    const auto pix = QVector3D(pixel.x(), _viewport.height() - pixel.y(), 1.0);
    const auto V = _camera->viewMatrix();
    const auto P = _camera->projectionMatrix();
    const auto originWorld = origin.unproject(V, P, _viewport);
    const auto pixelWorld = pix.unproject(V, P, _viewport);
    Ray ray { originWorld, pixelWorld };
    ray.getDirection().normalize();
    return ray;
}
}
