#include "Layers.h"
#include "MyCameraController.h"
#include "RootEntity.h"
#include <QGuiApplication>
#include <QOpenGLContext>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickView>

using namespace ibh;

int main(int argc, char** argv)
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<Layers>("custom", 1, 0, "Layers");
    qmlRegisterType<MyCameraController>("custom", 1, 0, "MyCameraController");
    qmlRegisterType<RootEntity>("custom", 1, 0, "RootEntity");

    QSurfaceFormat format;
    if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL) {
        format.setVersion(3, 3);
        format.setProfile(QSurfaceFormat::CoreProfile);
    }
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);


    QQuickView view;
    view.resize(1280, 720);
    view.setFormat(format);
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl("qrc:/main.qml"));
    view.setColor("#000000");
    view.show();

    return app.exec();
}
