#pragma once
#include "Intersectable.h"
#include <QVector3D>

namespace ibh {
class Ray;

class Disc : public Intersectable {
private:
    QVector3D _origin;
    QVector3D _normal;
    float _radius = 1.0f;

public:
    Disc() = default;
    Disc(const QVector3D& origin, const QVector3D& normal, float radius);
    QVector3D& origin() { return _origin; }
    const QVector3D& origin() const { return _origin; }
    void setOrigin(const QVector3D& origin) { _origin = origin; }
    QVector3D& normal() { return _normal; }
    const QVector3D& normal() const { return _normal; }
    void setNormal(const QVector3D& normal) { _normal = normal; }
    float radius() { return _radius; }
    void setRadius(float radius) { _radius = radius; }
    void normalize();
    QVector3D intersect(const Ray& ray) const override;
};
}
