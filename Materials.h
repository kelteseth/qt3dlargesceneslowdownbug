#pragma once
#include <QVector>

namespace Qt3DExtras {
class QPhongMaterial;
}
namespace ibh {

class Materials {
private:
    Qt3DExtras::QPhongMaterial* _green = nullptr;
    Qt3DExtras::QPhongMaterial* _red = nullptr;
    Qt3DExtras::QPhongMaterial* _yellow = nullptr;
    Qt3DExtras::QPhongMaterial* _blue = nullptr;

public:
    Qt3DExtras::QPhongMaterial* green();
    Qt3DExtras::QPhongMaterial* red();
    Qt3DExtras::QPhongMaterial* yellow();
    Qt3DExtras::QPhongMaterial* blue();
    QVector<Qt3DExtras::QPhongMaterial*> materials();
};

}
