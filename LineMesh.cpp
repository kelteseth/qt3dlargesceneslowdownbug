#include "LineMesh.h"
#include <QByteArray>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QGeometry>

namespace ibh {
LineMesh::LineMesh(Qt3DCore::QNode* parent)
    : Qt3DRender::QGeometryRenderer(parent)
{
    // Vertex
    QByteArray vertexArray;
    vertexArray.resize(6 * sizeof(float));
    auto* floatPtr = reinterpret_cast<float*>(vertexArray.data());
    *floatPtr++ = 0.0f;
    *floatPtr++ = 0.0f;
    *floatPtr++ = 0.0f;
    *floatPtr++ = 1.0f;
    *floatPtr++ = 0.0f;
    *floatPtr++ = 0.0f;

    auto* vertexBuffer = new Qt3DRender::QBuffer();
    vertexBuffer->setType(Qt3DRender::QBuffer::BufferType::VertexBuffer);
    vertexBuffer->setAccessType(Qt3DRender::QBuffer::AccessType::Read);
    vertexBuffer->setUsage(Qt3DRender::QBuffer::UsageType::StaticDraw);
    vertexBuffer->setData(vertexArray);

    auto* vertexAttribute = new Qt3DRender::QAttribute();
    vertexAttribute->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
    vertexAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    vertexAttribute->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
    vertexAttribute->setVertexSize(3);
    vertexAttribute->setByteOffset(0);
    vertexAttribute->setByteStride(3 * sizeof(float));
    vertexAttribute->setCount(2);
    vertexAttribute->setBuffer(vertexBuffer);

    // Index
    QByteArray indexArray;
    indexArray.resize(2 * sizeof(unsigned int)); // start to end
    unsigned int* uintPtr = reinterpret_cast<unsigned int*>(indexArray.data());
    *uintPtr++ = 0;
    *uintPtr++ = 1;

    auto* indexBuffer = new Qt3DRender::QBuffer();
    indexBuffer->setType(Qt3DRender::QBuffer::BufferType::IndexBuffer);
    indexBuffer->setAccessType(Qt3DRender::QBuffer::AccessType::Read);
    indexBuffer->setUsage(Qt3DRender::QBuffer::UsageType::StaticDraw);
    indexBuffer->setData(indexArray);

    auto* indexAttribute = new Qt3DRender::QAttribute();
    //vertexAttribute->setName(Qt3DRender::QAttribute::defaultJointIndicesAttributeName());
    indexAttribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    indexAttribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    indexAttribute->setCount(2);
    indexAttribute->setBuffer(indexBuffer);

    // Geometry
    auto geometry = new Qt3DRender::QGeometry();
    geometry->addAttribute(vertexAttribute);
    geometry->addAttribute(indexAttribute);

    // Entity
    setPrimitiveType(PrimitiveType::Lines);
    setGeometry(geometry);
}
}
