### Qt 3D slowdown in a large scene

https://bugreports.qt.io/browse/QTBUG-75492

Qt 5.12:
Tested with:

PC 1:

- AMD Athlon 200GE with Radeon Vega Graphics
- Ubuntu 19.04 mesa drivers
- Runs very well

PC2:

- AMD Ryzen 2700 + AMD Radeon 570
- Windows 10 + AMD proprietary drivers
- Runs slow when looking at all barells

PC3:

- Intel i7 7700 + Nvidia 770
- Windows 10 + Nvidia  proprietary drivers
- Runs slow when looking at all barells

PC4:

- Intel i7 7700 + Nvidia 840m
- Ubuntu 18.04 + Nvidia  proprietary drivers
- Runs slow when looking at all barells
- Ubuntu 18.04 + Mesa
- Runs very well
