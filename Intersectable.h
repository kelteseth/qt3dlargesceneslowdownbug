#pragma once

class QVector3D;

namespace ibh {
class Ray;

class Intersectable {
public:
    virtual ~Intersectable() = default;
    virtual QVector3D intersect(const Ray& ray) const = 0;
};
}
