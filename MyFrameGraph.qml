import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import custom 1.0

RenderSurfaceSelector {
    id: root
    property bool frustumCullingEnabled: true
    property bool faceCullingEnabled: true
    property bool depthTestEnabled: true
    property var layers: []
    property Camera camera

    Viewport {
        id: mainViewport
        normalizedRect: Qt.rect(0, 0, 1, 1)
        ClearBuffers {
            buffers: ClearBuffers.ColorDepthBuffer
            clearColor: "white"
            CameraSelector {
                id: cameraSelector
                camera: root.camera
                LayerFilter {
                    layers: root.layers
                    FrustumCulling {
                        enabled: frustumCullingEnabled
                        RenderStateSet {
                            renderStates: [
                                DepthTest {
                                    enabled: depthTestEnabled
                                    depthFunction: DepthTest.Less
                                },
                                CullFace {
                                    enabled: faceCullingEnabled
                                    mode: CullFace.Back
                                }
                            ]
                        }
                    }
                }
            }
        }
    }
}
