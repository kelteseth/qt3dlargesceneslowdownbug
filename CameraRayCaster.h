#pragma once
#include "Intersectable.h"
#include <Qt3DCore/QComponent>
#include <Qt3DRender/QAbstractRayCaster>
#include <Qt3DRender/QCamera>

namespace ibh {
class Ray;

class CameraRayCaster : public Qt3DCore::QComponent {
    Q_OBJECT
    Q_PROPERTY(Qt3DRender::QCamera* camera READ camera WRITE setCamera NOTIFY cameraChanged)
    Q_PROPERTY(QRect viewport READ viewport WRITE setViewport NOTIFY viewportChanged)
    Q_PROPERTY(Intersectable* entity READ entity WRITE setEntity NOTIFY entityChanged)

private:
    Qt3DRender::QCamera* _camera = nullptr;
    QRect _viewport;
    Intersectable* _entity = nullptr;

public:
    CameraRayCaster() = default;
    Qt3DRender::QCamera* camera() const;
    void setCamera(Qt3DRender::QCamera* camera);
    QRect viewport() const;
    void setViewport(const QRect& viewport);
    Intersectable* entity() const;
    void setEntity(Intersectable* entity);
    void trigger(const QPoint& pixel);

signals:
    void cameraChanged();
    void viewportChanged();
    void entityChanged();
    void hitsChanged(const Qt3DRender::QAbstractRayCaster::Hits& hits);

private:
    Ray createRay(const QPoint& pixel) const;
};
}
