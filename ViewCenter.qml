import Qt3D.Core 2.12
import Qt3D.Extras 2.12
import Qt3D.Render 2.12

Entity {
    id: root
    property Camera camera
    property Layer layer 
    SphereMesh {
        id: mesh
        slices: 10
        rings: 10
        radius: 1
    }
    Transform {
        id: transform
        translation: camera.viewCenter
    }
    PhongMaterial {
        id: material
        diffuse: "white"
        ambient: "grey"
    }
    components: [mesh, transform, material, layer]
}
