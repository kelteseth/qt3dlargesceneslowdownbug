#include "Layers.h"

namespace ibh {
Layers::Layers(Qt3DCore::QNode* parent)
    : Qt3DCore::QNode(parent)
{
}

Qt3DRender::QLayer* Layers::debugLayer()
{
    //if (!m_debugLayer) {
    //    m_debugLayer = new Qt3DRender::QLayer();
    //    m_debugLayer->setRecursive(true);
    //    emit debugLayerChanged(m_debugLayer);
    //}

    return m_debugLayer;
}

Qt3DRender::QLayer* Layers::sceneLayer()
{
    //if (!m_sceneLayer) {
    //    m_sceneLayer = new Qt3DRender::QLayer();
    //    m_sceneLayer->setRecursive(true);
    //    emit sceneLayerChanged(m_sceneLayer);
    //}

    return m_sceneLayer;
}

Qt3DRender::QLayer* Layers::objectLayer()
{
    //if (!m_objectLayer) {
    //    m_objectLayer = new Qt3DRender::QLayer();
    //    m_objectLayer->setRecursive(true);
    //    emit objectLayerChanged(m_objectLayer);
    //}

    return m_objectLayer;
}

Qt3DRender::QLayer* Layers::osmLayer()
{
    //if (!m_osmLayer) {
    //    m_osmLayer = new Qt3DRender::QLayer();
    //    m_osmLayer->setRecursive(true);
    //    emit osmLayerChanged(m_osmLayer);
    //}

    return m_osmLayer;
}

Qt3DRender::QLayer* Layers::dxfLayer()
{
    //if (!m_dxfLayer) {
    //    m_dxfLayer = new Qt3DRender::QLayer();
    //    m_dxfLayer->setRecursive(true);
    //    emit dxfLayerChanged(m_dxfLayer);
    //}

    return m_dxfLayer;
}

Qt3DRender::QLayer* Layers::flagLayer()
{
    //if (!m_flagLayer) {
    //    m_flagLayer = new Qt3DRender::QLayer();
    //    m_flagLayer->setRecursive(true);
    //    emit flagLayerChanged(m_flagLayer);
    //}

    return m_flagLayer;
}

void Layers::setDebugLayer(Qt3DRender::QLayer* debugLayer)
{
    if (m_debugLayer == debugLayer)
        return;

    m_debugLayer = debugLayer;
    emit debugLayerChanged(m_debugLayer);
}

void Layers::setSceneLayer(Qt3DRender::QLayer* sceneLayer)
{
    if (m_sceneLayer == sceneLayer)
        return;

    m_sceneLayer = sceneLayer;
    emit sceneLayerChanged(m_sceneLayer);
}

void Layers::setObjectLayer(Qt3DRender::QLayer* objectLayer)
{
    if (m_objectLayer == objectLayer)
        return;

    m_objectLayer = objectLayer;
    emit objectLayerChanged(m_objectLayer);
}

void Layers::setOsmLayer(Qt3DRender::QLayer* osmLayer)
{
    if (m_osmLayer == osmLayer)
        return;

    m_osmLayer = osmLayer;
    emit osmLayerChanged(m_osmLayer);
}

void Layers::setDxfLayer(Qt3DRender::QLayer* dxfLayer)
{
    if (m_dxfLayer == dxfLayer)
        return;

    m_dxfLayer = dxfLayer;
    emit dxfLayerChanged(m_dxfLayer);
}

void Layers::setFlagLayer(Qt3DRender::QLayer* flagLayer)
{
    if (m_flagLayer == flagLayer)
        return;

    m_flagLayer = flagLayer;
    emit flagLayerChanged(m_flagLayer);
}
}
